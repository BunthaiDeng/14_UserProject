package com.hrd.spring.service.user;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hrd.spring.entity.User;
import com.hrd.spring.response.Pagination;

public interface UserService {
	public List<User> findAllUser(Pagination pagination, int limit, int offset);
	public int countAllUser();
	public User findUserByUUID(String uuid);
	public boolean updateUser(User user);
	public boolean updateUserStatusByUUID(String uuid, String status);
	public boolean deleteUserByUUID(String uuid);
	public boolean insertUser(User user);
	public User findUserByEmail(User user);
}
