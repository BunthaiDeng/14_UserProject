package com.hrd.spring.service.user.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrd.spring.entity.User;
import com.hrd.spring.repository.user.UserRepository;
import com.hrd.spring.response.Pagination;
import com.hrd.spring.service.user.UserService;

@Service
public class UserServiceImpl implements UserService{

	UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public List<User> findAllUser(Pagination pagination, int limit, int offset) {
		return userRepository.findAllUser(pagination, limit, offset);
	}

	@Override
	public User findUserByUUID(String uuid) {
		return userRepository.findUserByUUID(uuid);
	}

	@Override
	public boolean updateUser(User user) {
		return userRepository.updateUser(user);
	}

	@Override
	public boolean updateUserStatusByUUID(String uuid, String status) {
		return userRepository.updateUserStatusByUUID(uuid, status);
	}

	@Override
	public boolean deleteUserByUUID(String uuid) {
		return userRepository.deleteUserByUUID(uuid);
	}

	@Override
	public boolean insertUser(User user) {
		return userRepository.insertUser(user);
	}
	
	@Override
	public User findUserByEmail(User user) {
		return userRepository.findUserByEmail(user);
	}

	@Override
	public int countAllUser() {
		return userRepository.countAllUser();
	}
	
}
