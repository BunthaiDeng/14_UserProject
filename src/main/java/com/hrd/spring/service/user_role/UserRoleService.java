package com.hrd.spring.service.user_role;

import java.util.List;

import com.hrd.spring.entity.Role;

public interface UserRoleService {
	public boolean insertUserRole(List<Role> roles, int userid);
	public boolean deleteUserRoleByUserId(int userid);
}
