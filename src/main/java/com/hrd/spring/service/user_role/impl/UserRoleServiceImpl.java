package com.hrd.spring.service.user_role.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrd.spring.entity.Role;
import com.hrd.spring.repository.user_roles.UserRoleRepository;
import com.hrd.spring.service.user_role.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService{

	UserRoleRepository userRoleRepository;
	
	public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
		super();
		this.userRoleRepository = userRoleRepository;
	}

	@Override
	public boolean insertUserRole(List<Role> roles, int userid) {
		return userRoleRepository.insertUserRole(roles, userid);
	}

	@Override
	public boolean deleteUserRoleByUserId(int userid) {
		return userRoleRepository.deleteUserRoleByUserId(userid);
	}
	

}
