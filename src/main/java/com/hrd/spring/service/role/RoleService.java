package com.hrd.spring.service.role;

import java.util.List;

import com.hrd.spring.entity.Role;

public interface RoleService {
	public List<Role> findAllRole();
}
