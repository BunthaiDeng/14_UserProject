package com.hrd.spring.service.role.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrd.spring.entity.Role;
import com.hrd.spring.repository.role.RoleRepository;
import com.hrd.spring.service.role.RoleService;

@Service
public class RoleServiceImpl implements RoleService{

	RoleRepository roleRepository;
	
	@Autowired
	public RoleServiceImpl(RoleRepository roleRepository) {
		super();
		this.roleRepository = roleRepository;
	}

	@Override
	public List<Role> findAllRole() {
		return roleRepository.findAllRole();
	}


}
