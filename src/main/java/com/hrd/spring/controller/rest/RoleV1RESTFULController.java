package com.hrd.spring.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hrd.spring.entity.Role;
import com.hrd.spring.entity.User;
import com.hrd.spring.response.HttpMessage;
import com.hrd.spring.response.Pagination;
import com.hrd.spring.response.Response;
import com.hrd.spring.response.ResponseHttpStatus;
import com.hrd.spring.response.ResponseList;
import com.hrd.spring.response.Table;
import com.hrd.spring.response.Transaction;
import com.hrd.spring.response.failure.ResponseListFailure;
import com.hrd.spring.service.role.RoleService;

@RestController
@RequestMapping("/v1/api/role")
public class RoleV1RESTFULController {

	RoleService roleService;
	HttpStatus httpStatus = HttpStatus.OK;

	@Autowired
	public RoleV1RESTFULController(RoleService roleService) {
		super();
		this.roleService = roleService;
	}
	
	
	@GetMapping
	public ResponseEntity<ResponseList<Role>> findAllRole(){
		ResponseList<Role> responseList = new ResponseList<>();
		try{
			List<Role> roles = roleService.findAllRole();
			if(!roles.isEmpty()){
				responseList = new ResponseList<Role>(HttpMessage.success(Table.ROLES, Transaction.Success.RETRIEVE), 
													true, roles, null);
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseList = new ResponseListFailure<>(HttpMessage.fail(Table.ROLES, Transaction.Fail.RETRIEVE), false,
								ResponseHttpStatus.NOT_FOUND);
			}
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = httpStatus.INTERNAL_SERVER_ERROR;
			responseList = new ResponseListFailure<>(HttpMessage.fail(Table.ROLES, Transaction.Fail.RETRIEVE), false, 
							ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		
		return new ResponseEntity<ResponseList<Role>>(responseList, httpStatus);		
	}
	
//	@RequestMapping("/data")
//	public String roleFragment(ModelMap m){
//		m.addAttribute("roles", roleService.findAllRole());
//		return "user-list :: roleBlock";
//	}
	
	
}
