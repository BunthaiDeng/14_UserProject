package com.hrd.spring.controller.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hrd.spring.entity.User;
import com.hrd.spring.response.HttpMessage;
import com.hrd.spring.response.Pagination;
import com.hrd.spring.response.ResponseHttpStatus;
import com.hrd.spring.response.ResponseList;
import com.hrd.spring.response.ResponseRecord;
import com.hrd.spring.response.Table;
import com.hrd.spring.response.Transaction;
import com.hrd.spring.response.failure.ResponseFailure;
import com.hrd.spring.response.failure.ResponseListFailure;
import com.hrd.spring.response.failure.ResponseRecordFailure;
import com.hrd.spring.service.role.RoleService;
import com.hrd.spring.service.user.UserService;
import com.hrd.spring.service.user_role.UserRoleService;

@RestController
@RequestMapping("/v1/api/user")
public class UserV1RESTFULController {

	UserService userService;
	UserRoleService userRoleService;
	HttpStatus httpStatus = HttpStatus.OK;
	
	@Autowired
	public UserV1RESTFULController(UserService userService, UserRoleService userRoleService) {
		super();
		this.userService = userService;
		this.userRoleService = userRoleService;
	}

	@GetMapping("/find-all-user")//findAllUser
	public ResponseEntity<ResponseList<User>> findAllUser
			(
				@RequestParam(value = "page", required = false, defaultValue = "1") int page, 
				@RequestParam(value = "limit", required = false, defaultValue = "10") int limit
			){
//		System.out.println(page+", "+limit);
		ResponseList<User> responseList = new ResponseList<>();
		if(userService.countAllUser()<=0)
			return null;
		System.out.println("here: "+userService.countAllUser());
		try{
//			System.out.println(userService.countAllUser());
			Pagination pagination = new Pagination();
			pagination.setPage(page);
			pagination.setLimit(limit);
			int userTotal = userService.countAllUser();
//			if(userTotal == 0) userTotal = 1;
//			pagination.setTotalCount(userTotal);
			pagination.setTotalCount(userService.countAllUser());
			pagination.setTotalPages(pagination.getTotalPages());
			pagination.setOffset(pagination.offset());
			
			System.out.println(pagination);
			System.out.println("TotalCount: "+pagination.getTotalCount());
			System.out.println("TotalPage :"+pagination.getTotalPages());
			
			List<User> users = userService.findAllUser(pagination, pagination.getLimit(), 
														pagination.offset());
			if(!users.isEmpty()){
				responseList = new ResponseList<User>(HttpMessage.success(Table.USERS, Transaction.Success.RETRIEVE), 
													true, users, pagination);
				
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseList = new ResponseListFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE), false,
								ResponseHttpStatus.NOT_FOUND);
			}
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseList = new ResponseListFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE), false, 
							ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		
		return new ResponseEntity<ResponseList<User>>(responseList, httpStatus);
	}
	
	@GetMapping("/{uuid}")// findUserByUUID
	public ResponseEntity<ResponseRecord<User>> findUserByUUID(@PathVariable("uuid") String uuid){
		ResponseRecord<User> responseRecord = new ResponseRecord<>();
		try{
			User user = userService.findUserByUUID(uuid);
			if(user!=null){
				responseRecord = new ResponseRecord<User>(HttpMessage.success(Table.USERS, Transaction.Success.RETRIEVE), true, user);
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseRecord = new ResponseRecordFailure<User>(HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE), false, ResponseHttpStatus.NOT_FOUND);
			}
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<User>(HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE), false, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
	}
	
	@PutMapping //updateUser
	public ResponseEntity<ResponseRecord<User>> updateUser(@RequestBody User user){
		ResponseRecord<User> responseRecord = null;
		System.out.println(user);
		try{
			if(userService.updateUser(user)){
				int userId = userService.findUserByUUID(user.getUuid()).getId();
				
					System.out.println(user.getRoles());
					userRoleService.deleteUserRoleByUserId(userId);
					if(user.getRoles().size()>0)
					userRoleService.insertUserRole(user.getRoles(), userId);
				
				responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USER_ROLES, Transaction.Success.UPDATED), 
						 							true, userService.findUserByUUID(user.getUuid()));				
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),  
											false, ResponseHttpStatus.NOT_FOUND);
			}				
			
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USER_ROLES, Transaction.Fail.UPDATED), 
														 true, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
	}
	
	@PatchMapping("/status/{uuid}/{status}") //updateUserStatusByUUID
	public ResponseEntity<ResponseRecord<User>> updateUserStatusByUUID(@PathVariable("uuid") String uuid, @PathVariable("status") String status){
		ResponseRecord<User> responseRecord = null;
		System.out.println(uuid+", "+status);
		try{
			if(userService.updateUserStatusByUUID(uuid, status)){
				responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USER_ROLES, Transaction.Success.UPDATED), 
						 							true, userService.findUserByUUID(uuid));				
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.UPDATED),  
											false, ResponseHttpStatus.NOT_FOUND);
			}				
			
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USER_ROLES, Transaction.Fail.UPDATED), 
														 true, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
	}
	
	@GetMapping("/delete/{uuid}") //updateUserStatusByUUID
	public ResponseEntity<ResponseRecord<User>> deleteUserByUUID(@PathVariable("uuid") String uuid){
		ResponseRecord<User> responseRecord = null;
		System.out.println(uuid);
		try{
			User user = userService.findUserByUUID(uuid);
			userRoleService.deleteUserRoleByUserId(user.getId());
			System.out.println("Del here");
			if(userService.deleteUserByUUID(uuid)){
				responseRecord = new ResponseRecord<User>(HttpMessage.success(Table.USER_ROLES, Transaction.Success.DELETED), 
						 							true, user);				
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.DELETED),  
											false, ResponseHttpStatus.NOT_FOUND);
			}				
			
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USER_ROLES, Transaction.Fail.DELETED), 
														 true, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
		
	}	
	
	@PostMapping //insertUser
	public ResponseEntity<ResponseRecord<User>> insertUser(@RequestBody User user){
		ResponseRecord<User> responseRecord = null;
		user.setUuid(UUID.randomUUID().toString());
		user.setStatus("T");
		System.out.println(user);
		try{
			if(userService.insertUser(user)){
			
				System.out.println(user.getId());
				if(user.getRoles().size()!=0)
				userRoleService.insertUserRole(user.getRoles(), user.getId());
				responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USER_ROLES, Transaction.Success.CREATED), 
						 							true, userService.findUserByUUID(user.getUuid()));				
			}
			else{
				httpStatus = HttpStatus.NOT_FOUND;
				responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.CREATED),  
											false, ResponseHttpStatus.NOT_FOUND);
			}				
			
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USER_ROLES, Transaction.Fail.CREATED), 
														 true, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
	}
	
	@PostMapping("/find-user-by-email") //updateUserStatusByUUID
	public ResponseEntity<ResponseRecord<User>> findUserByEmail(@RequestBody User user){
		ResponseRecord<User> responseRecord = null;
		try{
			System.out.println(user);
			User myUser = userService.findUserByEmail(user);
			System.out.println(myUser);
			if(myUser != null){
				responseRecord = new ResponseRecord<>(HttpMessage.success(Table.USER_ROLES, Transaction.Success.RETRIEVE), 
						 							true, myUser);				
			}
			else{
//				httpStatus = HttpStatus.NOT_FOUND;
//				responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USERS, Transaction.Fail.RETRIEVE),  
//											false, ResponseHttpStatus.NOT_FOUND);
			}				
			
		}catch(Exception e){
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseRecord = new ResponseRecordFailure<>(HttpMessage.fail(Table.USER_ROLES, Transaction.Fail.RETRIEVE), 
														 true, ResponseHttpStatus.INTERNAL_SERVER_ERROR);
		}//catch
		return new ResponseEntity<ResponseRecord<User>>(responseRecord, httpStatus);
	}
	
	@GetMapping("/find-all-user/data")//find-all-user
	public ResponseEntity<ResponseList<User>> findAllUser1(){
		List<User> users = userService.findAllUser(null, 1, 5);
		return findAllUser(1, users.size());
	}	
	
	@GetMapping("/pagination")
	public Map<String, Pagination> getPagination(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page, 
			@RequestParam(value = "limit", required = false, defaultValue = "5") int limit			
			){
		
		
		Map<String, Pagination>  map = new HashMap<String, Pagination>();
		Pagination pagination = new Pagination();
		if(userService.countAllUser() == 0)
		{
			pagination.setTotalPages(0);
			map.put("pagination", pagination);
		}
		pagination.setPage(page);
		pagination.setLimit(limit);
		pagination.setTotalCount(userService.countAllUser());
		pagination.setTotalPages(pagination.getTotalPages());
		pagination.setOffset(pagination.offset());		
		
		
		map.put("pagination", pagination);
		return map;
	}
}
