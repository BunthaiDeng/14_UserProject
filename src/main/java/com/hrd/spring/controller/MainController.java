package com.hrd.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hrd.spring.entity.User;
import com.hrd.spring.service.user.UserService;


@Controller
public class MainController {

	@JsonProperty("geonames")
	private List<User> users;	
	
	@RequestMapping("/login")
	public String getLogin(){
		return "login";
	}
	
	
	@RequestMapping({"/","/admin", "/dashboard"})
	public ModelAndView getDashBoard(ModelAndView model){
		model.addObject("pic", "d1.jpg");
		model.addObject("name", "Dr. Steve Gection");
		model.addObject("page","dashboard");
		model.setViewName("dashboard");

//		RestTemplate restTemplate = new RestTemplate();
//		users = restTemplate.getForObject("http://localhost:8080/v1/api/user/find-all-user/data", List.class);
		
//		int total = 0, totalM = 0, totalF = 0;
//		for(User u : users){
//			
//			if("M".equals(u.getGender()))
//				totalM++;
//			else
//				totalF++;
//			
//			total++;
//		}
//		
//		model.addObject("total", total);
//		model.addObject("totalM", totalM);
//		model.addObject("totalF", totalF);
		
		return model;
	}
	
	
	@RequestMapping("/user-list")
	public ModelAndView getUserList(ModelAndView model){
		model.addObject("pic", "d1.jpg");
		model.addObject("name", "Dr. Steve Gection");		
		model.setViewName("user-list");
		model.addObject("page","userlist");

//		model.addObject("data", userService.findAllUser());

		return model;
	}
//	
//	@RequestMapping("/user-cu")
//	public ModelAndView getUserCU(ModelAndView model){
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("user-cu");
//		model.addObject("page","usercu");
//		return model;
//	}
//	
//	@RequestMapping(value = "/user-cu", method=RequestMethod.POST)
//	public ModelAndView postUserCU(@ModelAttribute User user, ModelAndView model){
//		user.setUserhash(UUID.randomUUID().toString());
//		System.out.println(user);
//		userService.addUser(user);
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("user-cu");
//		model.addObject("page","usercu");
////		model.addObject("redir", new ModelAndView("redirect:/user-list"));
//		return model;
//	}
//	
////	@RequestMapping(value = "/user-cu", method=RequestMethod.POST)
////
////	public ModelAndView postUserCU(User user){
////		System.out.println(user.getUsername());
////		return new ModelAndView("redirect:/user-list");
////	}	
//	
//	@RequestMapping("/user-update/{userhash}")
//	public ModelAndView getUserUpdate(ModelAndView model,@PathVariable("userhash") String userhash){
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("user-update");
//		model.addObject("page","userupdate");
//		User user = userService.findUserByUserHash(userhash);
//		model.addObject("username", user.getUsername());
//		model.addObject("email", user.getEmail());
//		model.addObject("gender", user.getGender());
//		model.addObject("phonenumber", user.getPhonenumber());
//		model.addObject("userhash", userhash);
//		return model;
//	}
//	
//	@RequestMapping("/user-profile/{userhash}")
//	public ModelAndView getUserProfile(ModelAndView model, @PathVariable("userhash") String userhash){
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("user-profile");
//		model.addObject("page","userupdate");
//		User user = userService.findUserByUserHash(userhash);
//		model.addObject("username", user.getUsername());
//		model.addObject("email", user.getEmail());
//		model.addObject("gender", user.getGender());
//		model.addObject("phonenumber", user.getPhonenumber());
//		model.addObject("userhash", userhash);
//		return model;
//	}		
//	
//	@RequestMapping(value = "/user-update", method=RequestMethod.POST)
//	public void postUserUpdate(@ModelAttribute User user){
////		System.out.println(user);
//		userService.updateUser(user);
//	}
//	
//	@RequestMapping(value = "/{userhash}")
//	public ModelAndView deleteUser(@PathVariable("userhash") String userhash, ModelAndView model){
//		userService.deleteUser(userhash);
//		
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("user-list");
//		model.addObject("page","userlist");
//
//		model.addObject("data", userService.findAllUser());
//
//		return model;
//	}
//	
////	@RequestMapping(value = "/user-cu", method=RequestMethod.POST)
////	@ResponseBody
////	public User postUserCU(@ModelAttribute User user){
//////		userService.addUser(user);
////		return user;
////	}	
//	
//	
//	
//	@RequestMapping("/role-cu")
//	public ModelAndView getRoleCU(ModelAndView model){
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("role-cu");
//		model.addObject("page","rolecu");
//		return model;
//	}
//	
//	@RequestMapping("/role-list")
//	public ModelAndView getRoleList(ModelAndView model){
//		model.addObject("pic", "d1.jpg");
//		model.addObject("name", "Dr. Steve Gection");		
//		model.setViewName("role-list");
//		model.addObject("page","rolelist");
//		model.addObject("data", userService.findAllUser());
//		return model;
//	}	
//	
//	@RequestMapping("/user")
//	public ModelAndView getUser(ModelAndView model){
//		model.addObject("pic", "6.jpg");
//		model.addObject("name", "Jane");
//		model.setViewName("user");
//		
//		return model;
//	}
//	
//	
}
