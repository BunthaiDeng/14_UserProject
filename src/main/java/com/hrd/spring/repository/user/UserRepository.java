package com.hrd.spring.repository.user;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.hrd.spring.entity.Role;
import com.hrd.spring.entity.User;
import com.hrd.spring.response.Pagination;

@Repository
public interface UserRepository {

	@Select("select "
			+ "id,"
			+ "username, "
			+ "email, "
			+ "password, "
			+ "dob, "
			+ "gender, "
			+ "device, "
			+ "remark, "
			+ "status, "
			+ "uuid "
			+ "from  users order by id desc limit #{pagination.limit} offset #{pagination.offset}") //users where status='T'
	@Results
	(
		value = 
		{
			@Result(property="id", column="id"),
			@Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
		}// value
	)//results
	public List<User> findAllUser(@Param("pagination") Pagination pagination, int limit, int offset);
	
	@Select("select count(id) from users")
	public int countAllUser();
	
	@Select("select "
			+ "id, "
			+ "name, "
			+ "remark, "
			+ "status, "
			+ "uuid "
			+ "from user_roles ur join roles r on ur.role_id = r.id where ur.user_id = #{userid}")
	public List<Role> findRolesById(@Param("userid") int userid);
	
	@Select("select "
			+ "id,"
			+ "username, "
			+ "email, "
			+ "password, "
			+ "dob, "
			+ "gender, "
			+ "device, "
			+ "remark, "
			+ "status, "
			+ "uuid "
			+ "from users where status='T' and uuid = #{uuid}")
	@Results
	(
		value = 
		{
			@Result(property="id", column="id"),
			@Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
		}// value
	)//results
	public User findUserByUUID(@Param("uuid") String uuid);
	
	@Update("update users set username = #{user.username}, "
			+ "email = #{user.email}, "
			+ "password=#{user.password}, "
			+ "dob=#{user.dob}, "
			+ "gender=#{user.gender}, "
			+ "device=#{user.device} "
			+ "where uuid = #{user.uuid}"
			+ "")
	public boolean updateUser(@Param("user") User user);
	
	@Update("Update users set status = #{status} where uuid = #{uuid}")
	public boolean updateUserStatusByUUID(@Param("uuid") String uuid, @Param("status") String status);
	
	@Delete("delete from users where uuid = #{uuid}")
	public boolean deleteUserByUUID(@Param("uuid") String uuid);
	
	@Insert("insert into users(username, email, password, dob, gender, device, remark, status, uuid) "
			+ "values (#{user.username}, #{user.email}, #{user.password}, #{user.dob}, #{user.gender}, #{user.device}, #{user.remark}, "
			+ "#{user.status}, #{user.uuid})"
			+ "")
	@SelectKey
	(
		statement = "select last_value from users_id_seq", 
		keyProperty = "user.id",
		keyColumn = "last_value",
		before=false,
		resultType=int.class
	)
	public boolean insertUser(@Param("user") User user);
	
	@Select("select "
			+ "id,"
			+ "username, "
			+ "email, "
			+ "password, "
			+ "dob, "
			+ "gender, "
			+ "device, "
			+ "remark, "
			+ "status, "
			+ "uuid "
			+ "from users where status='T' and email = #{user.email} and password = #{user.password}")
	@Results
	(
		value = 
		{
			@Result(property="id", column="id"),
			@Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
		}// value
	)//results
	public User findUserByEmail(@Param("user") User user);
	
}
