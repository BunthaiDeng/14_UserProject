package com.hrd.spring.repository.user_roles;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.hrd.spring.entity.Role;

@Repository
public interface UserRoleRepository {

//	@Update("<script> "
//			+ "<foreach collection='roles' item='role' separator=','>"
//			+ "update user_roles set role_id=#{role.id} "
//			+ "where user_id = #{userid} and role_id=#{} "
//			+ "</foreach> "
//			+ "</script>")
	@Insert("<script> "
			+ "insert into user_roles "
			+ "values "
			+ "<foreach collection = 'roles' item='role' separator=','> ( "
			+ "#{userid}, "
			+ "#{role.id}) "
			+ "</foreach>"
			+ "</script> "
			+ "")
	public boolean insertUserRole(@Param("roles") List<Role> roles,@Param("userid") int userid);
	
	@Delete("delete from user_roles where user_id = #{userid}")
	public boolean deleteUserRoleByUserId(@Param("userid") int userid);
	
}
