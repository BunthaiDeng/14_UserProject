package com.hrd.spring.repository.role;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.hrd.spring.entity.Role;
import com.hrd.spring.entity.User;

@Repository
public interface RoleRepository {

	@Select("select id, name, remark, status, uuid from roles")
	public List<Role> findAllRole();
	
}
