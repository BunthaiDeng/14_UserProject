$(document).ready(function(){
	
	$("#email").focus(function(){
		$(this).css("border", "1px solid gray");
	});
	$("#password").focus(function(){
		$(this).css("border", "1px solid gray");
	});
	
	$('#login').click(function(){
		var myData = {
				"email" : $("#email").val(),
				"password" : $("#password").val()
		};
		$.ajax({
			url: "http://localhost:8080/v1/api/user/find-user-by-email",
			type: "POST",
			data: JSON.stringify(myData), 
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        success: function(data){
	        	if(data.status == true)
        		{
	        		swal("Good job!", "Login Successful!", "success");
	        		window.setTimeout(function(){
	        			location.href = "http://localhost:8080/dashboard";     
	                     }, 1200);
	        		
        		}
	        	else
        		{
	        		$("#email").css("border", "1px solid red");
	        		$("#password").css("border", "1px solid red");
	        		swal({
	        			  title: "Email or Password may not correct!",
	        			  type: "warning",
	        			  confirmButtonColor: "#DD6B55",
	        			  confirmButtonText: "OK",
	        			},
	        			function(){
	        				
	        			});
        		}
	        	
	        },
	        error:function(data,status,er) { 
		         console.log(data);
		    }
		});//ajax
	});//clickLogin
	
});//ready