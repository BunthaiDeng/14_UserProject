$(document).ready(function(){
	
	$("#save").click(function(){
		saveUser();
	});//save
	
	$('#add').click(function(){
		findAllRole("#roleBlock");
	});//add
	
	$(document).on('click', "#updateSave",function(){
		updateUser();
	});//updateSave
	
	
	$(document).on('click', "#updateStatus", function(){updateUserStatusByUUID(this)});
	$(document).on('click', "#btnDetail", function(){ findUserByUUID(this)});
	$(document).on('click', "#btnDelete", function(){deleteUserByUUID(this)});
	

	
	$(document).ready(function(){
		$(document).on('mousedown', "#btnUpdate", function(){
			console.log("s");
			findAllRole("#roleBlockUpdate");
			findUserByUUIDToUpdate(this);	
	
		});
	});


	
	
	var myCurrentPage = 1;
	var limit = 5;
	var check = true;
	
	findAllUser(myCurrentPage, limit);
	
	var myUser;
	function countAllUser(){
		$.ajax({
			url: "http://localhost:8080/v1/api/user/pagination",
			type: "GET",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        async: false,
	        success: function(data){
	        	myUser = data.pagination;
	        },
		    error:function(data,status,er) { 
		         console.log(data);
		    }		        
				
		});//ajax	
	}
	
	function deleteUserByUUID(self){
		
		swal({
			  title: "Are you sure to delete the user??",
			  text: "You will not be able to recover this imaginary file!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Yes, delete it!"
			}).then(function(json_data) {
				var uuid = $(self).data("uuid");
				$.ajax({
					url: "http://localhost:8080/v1/api/user/delete/"+uuid,
					type: "GET",
				    beforeSend: function(xhr) {
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
			        success: function(data){
			        		
			        	var count = $("tbody#userData").children('tr');
//			        	console.log(count.length);
			        	if(count.length-1<=1)
			        		myCurrentPage--;
			        	countAllUser();
			        	
			        	console.log(myUser);
//			        	console.log(count.length);
//			        	findAllUser(myCurrentPage, limit);
			        	if(myCurrentPage == 0) myCurrentPage = 1;
			        	setPagination(myUser.total_pages, limit);
			        	findAllUser(myCurrentPage, limit);
			        },
				    error:function(data,status,er) { 
				         console.log(data);
				    }		        
						
				});//ajax	
				swal("Deleted!", "Your imaginary file has been deleted.", "success");
			}, function(dismiss) {
			  if (dismiss === 'cancel') { // you might also handle 'close' or 'timer' if you used those
			    // ignore
			  } else {
			    throw dismiss;
			  }
			});		
		
//		swal({
//			  title: "Are you sure to delete the user??",
//			  text: "You will not be able to recover this imaginary file!",
//			  type: "warning",
//			  showCancelButton: true,
//			  confirmButtonColor: "#DD6B55",
//			  confirmButtonText: "Yes, delete it!"
//			},
//			function(){
//				var uuid = $(self).data("uuid");
//				$.ajax({
//					url: "http://localhost:8080/v1/api/user/delete/"+uuid,
//					type: "GET",
//				    beforeSend: function(xhr) {
//			            xhr.setRequestHeader("Accept", "application/json");
//			            xhr.setRequestHeader("Content-Type", "application/json");
//			        },
//			        success: function(data){
//			        	findAllUser(myCurrentPage, limit);
//			        },
//				    error:function(data,status,er) { 
//				         console.log(data);
//				    }		        
//						
//				});//ajax					
//				swal("Deleted!", "Your imaginary file has been deleted.", "success");
//			});//swal		
		
	
	}
	
	function findUserByUUIDToUpdate(self){
		var uuid = $(self).data("uuid");
		$.ajax({
//			$('#results').load("http://localhost:8080/v1/api/role/data");
			
		    url:  "http://localhost:8080/v1/api/user/" + uuid, 
		    type: "GET",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
		    success: function(data) {
//	    		console.log(data);
		    	$("#updateuuid").val(data.data.uuid);
	    		$("#updateusername").val(data.data.username);
	    		$("#updateemail").val(data.data.email);
	    		$("#updatepassword").val(data.data.password);
	    		$("#updategender").val(data.data.gender);
	    		$("#updatedob").val(data.data.dob);

				var roleTags = $('#roleBlockUpdate').children('label').children('input');
		    	var roles = data.data.roles;
		    	for(var i=0; i<roleTags.length; i++){
		    		for(var j=0; j<roles.length; j++){
		    			if(roleTags[i].getAttribute("data-id")==roles[j].id)
		    			{
		    				roleTags[i].checked = true;
		    				break;
		    			}
		    		}

		    	}
		    },
		    error:function(data,status,er) { 
		         console.log(data);
		    }	
		});//ajax
	}	
	
	function findUserByUUID(self){
		var uuid = $(self).data("uuid");
		$.ajax({
//			$('#results').load("http://localhost:8080/v1/api/role/data");
			
		    url:  "http://localhost:8080/v1/api/user/" + uuid, 
		    type: "GET",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
		    success: function(data) {
	    		console.log(data);
	    		var roleHTML = data.data.roles.length == 0 ? "<tr><td colspan=2>none</td></tr>" : "";
	    		for(var i = 0; i < data.data.roles.length; i++)
    			{
	    			roleHTML += "<tr>" +
									"<td colspan=2><i>" + data.data.roles[i].name+ "</i></td>" +
	    						"</tr>"
    			}
	    		swal({
	    			  title: "User's Detail",
	    			  html:"<div class='table-responsive'>" +
		    			  		"<table class='table'>" +
			    			  		"<tr>" +
				    			  		"<td><b>Name:</b> </td>" +
				    			  		"<td>" +data.data.username +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr>" +
				    			  		"<td><b>Email:</b> </td>" +
				    			  		"<td>" +data.data.email +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr>" +
				    			  		"<td><b>Gender:</b> </td>" +
				    			  		"<td>" +data.data.gender +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr>" +
				    			  		"<td><b>Date of Birth:</b> </td>" +
				    			  		"<td>" +data.data.dob +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr>" +
				    			  		"<td><b>Status:</b> </td>" +
				    			  		"<td>" +data.data.status +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr>" +
				    			  		"<td><b>Device:</b> </td>" +
				    			  		"<td>" +data.data.device +"</td>" +
			    			  		"</tr>" +	
			    			  		"<tr>" +
				    			  		"<td><b>Password:</b> </td>" +
				    			  		"<td>" +data.data.password +"</td>" +
			    			  		"</tr>" +
			    			  		"<tr><td colspan=2><b><u>Role</b></u></td></tr>" + roleHTML +			    			  		
		    			  		"</table>" +
	    			  		"</div>",
	    			  animation: true
	    			});    		
		    	
		    },
		    error:function(data,status,er) { 
		         console.log(data);
		    }	
		});//ajax
	}
	
	function updateUserStatusByUUID(self){
		var uuid = $(self).data("uuid");
		var status = $(self).data("status");
//			alert(uuid, status);
		$.ajax({
			url: "http://localhost:8080/v1/api/user/status/"+uuid+"/"+status,
			type: "PATCH",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        success: function(data){
	        	findAllUser(myCurrentPage, limit);
	        },
		    error:function(data,status,er) { 
		         console.log(data);
		    }		        
				
		});//ajax
	}
	
	function findAllUser(page, limit){
		$.ajax({
//			$('#results').load("http://localhost:8080/v1/api/role/data");
			
		    url:  "http://localhost:8080/v1/api/user/find-all-user?page="+page+"&"+"limit="+limit, 
		    type: "GET",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
		    success: function(data) { 
				console.log(data.data);
				$("tbody#userData").empty();
				$("#user-tmpl").tmpl({users: data.data}).appendTo("tbody#userData");
				if(check){
					setPagination(data.pagination.total_pages, page)
					check = false;
				}
		    	
		    },
		    error:function(data,status,er) { 
		         console.log(data);
		    }		
		});//ajax		
	}//findAllUser		
	
	function setPagination(totalPage, currentPage){
		$('#pagination').bootpag({
		    total: totalPage
		}).on("page", function(event, currentPage){
		    check = false;
		    myCurrentPage = currentPage;
		    findAllUser(currentPage, limit);
		 
		});			
	}//setPagination
	
	function updateUser(){
		
		swal({
			  title: "Updaute user's data?",
			  text: "Submit to run ajax request",
			  type: "info",
			  showCancelButton: true,
			  showLoaderOnConfirm: true,
			}).then(function(json_data) {
				  setTimeout(function(){
						var roleTags = $('#roleBlockUpdate').children('label').children('input');
						
						var myRoles = [];
						for(var i = 0; i<roleTags.length; i++){
							if(roleTags[i].checked==true){
//								alert(roleTags[i].getAttribute('data-id'));
								var obj = new Object();
								obj.id = roleTags[i].getAttribute('data-id');
								myRoles[myRoles.length] = obj;
							}
						}
						console.log(myRoles);
//						alert($("#updateuuid").data("id"));
						var myData = 
						{
						
								
								"uuid" : $("#updateuuid").val(),
								"username" : $("#updateusername").val(),
								"email" : $("#updateemail").val(),
								"password" : $("#updatepassword").val(),
								"gender" : $("#updategender").val(),
								"dob" : "2018-08-08",
								"roles" : myRoles 
						};
						
						console.log(myData);
						
						$.ajax({
						    url:  "http://localhost:8080/v1/api/user/", 
						    type: "PUT",
						    data: JSON.stringify(myData),
						    beforeSend: function(xhr) {
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
					        },
						    success: function(data) { 
								console.log(data);
								clear();
								findAllUser(myCurrentPage, limit);
						    	
						    },
						    error:function(data,status,er) { 
						         console.log(data);
						    }		
						});//ajax							  
				    swal("Ajax request finished!");
				  }, 500);//SetTimtout
			}, function(dismiss) {
			  if (dismiss === 'cancel') { // you might also handle 'close' or 'timer' if you used those
			    // ignore
			  } else {
			    throw dismiss;
			  }
			});		
		
//		swal({
//			  title: "Add user's data?",
//			  text: "Submit to run ajax request",
//			  type: "info",
//			  showCancelButton: true,
//			  closeOnConfirm: false,
//			  showLoaderOnConfirm: true,
//			},
//
//			function(){
//
//			});//swal		
			
	}//updateUser	
	
	
	function saveUser(){
		
		swal({
			  title: "Add user's data?",
			  text: "Submit to run ajax request",
			  type: "info",
			  showCancelButton: true,
			  showLoaderOnConfirm: true,
			}).then(function(json_data) {
				  setTimeout(function(){
						var roleTags = $('#roleBlock').children('label').children('input');
						
						var myRoles = [];
						for(var i = 0; i<roleTags.length; i++){
							if(roleTags[i].checked==true){
//								alert(roleTags[i].getAttribute('data-id'));
								var obj = new Object();
								obj.id = roleTags[i].getAttribute('data-id');
								myRoles[myRoles.length] = obj;
								roleTags[i].checked = false;
							}
						}
						console.log(myRoles);
						var myData = 
						{
								"username" : $("#username").val(),
								"email" : $("#email").val(),
								"password" : $("#password").val(),
								"gender" : $("#gender").val(),
								"dob" : "2018-08-08",
								"roles" : myRoles 
						};
						
//						console.log(myData);
						
						$.ajax({
						    url:  "http://localhost:8080/v1/api/user", 
						    type: "POST",
						    data: JSON.stringify(myData),
						    beforeSend: function(xhr) {
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
					        },
						    success: function(data) { 
//								console.log(data);
						    	
						    	countAllUser();
					        	var count = $("tbody#userData").children();
					        	console.log(count.length);
					        	if(count.length == limit)
					        		myCurrentPage++;
					        	
					        	console.log(myUser.total_pages+", limit"+ limit+", current:"+ myCurrentPage);
					        	$("#pagination").empty();
					        	setPagination(myUser.total_pages, limit);
								findAllUser(myCurrentPage, limit);	
								clear();

						    },
						    error:function(data,status,er) { 
						         console.log(data);
						    }		
						});//ajax							  
				    swal("Ajax request finished!");
				  }, 500);//SetTimtout
			}, function(dismiss) {
				
			  if (dismiss === 'cancel') { // you might also handle 'close' or 'timer' if you used those
			    // ignore
			  } else {
			    throw dismiss;
			  }
			});		
		
//		swal({
//			  title: "Add user's data?",
//			  text: "Submit to run ajax request",
//			  type: "info",
//			  showCancelButton: true,
//			  closeOnConfirm: false,
//			  showLoaderOnConfirm: true,
//			},
//
//			function(){
//
//			});//swal		
			
	}//saveUser	
	
	function findAllRole(block){
		$.ajax({
//			$('#results').load("http://localhost:8080/v1/api/role/data");
		    url:  "http://localhost:8080/v1/api/role", 
		    type: "GET",
		    beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
		    success: function(data) { 
				console.log(data);
				$(block).empty();
				for(var i = 0; i<data.data.length; i++){
					$(block).append("<label><input id="+data.data[i].id+" class='dd' data-id='"+data.data[i].id+"' type='checkbox'>"+data.data[i].name+"</label>")
					if(((i+1) % 3)==0)
						$(block).append("<br>");
					if(i<data.data.length-1 && ((i+1) % 3)!=0)
						$(block).append(" | ");
				}
		    	
		    },
		    error:function(data,status,er) { 
		         console.log(data);
		    }		
		});//ajax		
	}//findAllRole	
	
	function clear(){
		$("#username").val("");
		$("#email").val("");
		$("#password").val("");
		$("#gender").val("Select Gender")
		$("#dob").val("");
	}
	
		
	
});//ready